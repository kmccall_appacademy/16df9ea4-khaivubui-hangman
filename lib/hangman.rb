require 'byebug'

class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(options = {}) # take hash of referee and guesser
    @referee = options[:referee]
    @guesser = options[:guesser]
  end

  def setup # referee picks secret word, guesser receives length, board is set up
    secret_length = referee.pick_secret_word
    guesser.register_secret_length(secret_length)
    @board = Array.new(secret_length)
  end

  def take_turn # guesser guesses, ref checks guess, board is updated, guesser "handle_response"
    guess = guesser.guess(board)
    indexes = referee.check_guess(guess)
    update_board(guess, indexes)
    guesser.handle_response
  end

  def update_board(guess, indexes) # fill indexes with given guess
    indexes.each do |index|
      board[index] = guess
    end
  end

end

class HumanPlayer
  attr_reader :dictionary, :secret_word, :secret_length, :board,
              :candidate_words
end

class ComputerPlayer
  attr_reader :dictionary, :secret_word, :secret_length, :board,
              :candidate_words

  def initialize(dictionary) # accepts an array of words
    @dictionary = dictionary
    @candidate_words = dictionary
  end

  def pick_secret_word # assign random word from dict into @secret_word
    @secret_word = dictionary[rand(0..dictionary.length - 1)]
    secret_word.length
  end

  def alphabet # just an array of lowercase a - z
    ('a'..'z').to_a
  end

  def check_guess(letter) # return index(s) of letter
    letter.downcase!
    raise 'ERROR: not a letter' unless alphabet.include? letter
    letter_index_array = []
    secret_word.chars.each_with_index do |ch, i|
      letter_index_array << i if ch == letter
    end
    letter_index_array
  end

  def register_secret_length(length) #store length into @secret_length, update candidate_words
    @secret_length = length
    @candidate_words.select! { |word| word.length == length }
  end

  def guess(board) #produce an educated guess
    @board = board
    candidate_letters = candidate_words.join.chars
    candidate_letters.reject! { |l| board.include? l }
    candidate_letters.reduce('') do |acc, ch|
      if candidate_letters.count(ch) > candidate_letters.count(acc)
        ch
      else
        acc
      end
    end
  end

  def handle_response(guess, indexes) # filter candidate_words based on guess and indexes from check_guess
    @candidate_words.select!.each_with_index do |word, index|
      if indexes.include? index
        word[index] == guess
      else
        word[index] != guess
      end
    end
  end
end
